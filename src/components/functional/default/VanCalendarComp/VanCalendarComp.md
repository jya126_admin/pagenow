## this.Calendar

通过`this.Calendar`可以获取到日期组件的实例，通过实例可进行一系列函数操作，支持的函数如下：

**注意：this.Calendar指向的是（日期选择M）组件内所使用的日历组件的实例，而不是（日期选择M）组件自身实例**

### reset()

将选中的日期重置到指定日期，未传参时会重置到默认日期

 - Date | Date[]

### scrollToDate()

滚动到某个日期

 - Date
