import PnDesigner from "@/utils/PnDesigner";

const buildSeriesObj = function (name, type, yAxisIndex = 0) {
  return {
    name: name,
    type: type,
    yAxisIndex: yAxisIndex,
    label: {
      show: false,
      position: 'inside',
      color: '#fff',
      fontSize: 12
    },
    itemStyle: {
      color: '',
      borderColor: '#fff',
      borderWidth: 0,
      borderType: 'solid',
      shadowBlur: 8,
      shadowColor: ''
    },
    lineStyle: {
      type: 'solid',
      width: 2
    },
    areaStyle: {
      show: true,
      normal: {
        startColor: '',
        endColor: '',
        shadowColor: '#1565c0',
        shadowBlur: 20
      }
    },
    smooth: true, //平滑曲线显示
    symbol: "circle", //标记的图形为实心圆
    symbolSize: 0, //标记的大小
    markLine: PnDesigner.buildMarkLineConfigData(),
    markPoint: PnDesigner.buildMarkPointConfigData()
  }
};

const buildDefaultSeriesObj = function (name = '', data) {
  return {
    name: name,
    type: 'line',
    yAxisIndex: 0,
    label: {
      show: false,
      position: 'inside',
      color: '#fff',
      fontSize: 12
    },
    itemStyle: {
      color: '',
      borderColor: '#fff',
      borderWidth: 0,
      borderType: 'solid',
      shadowBlur: 8,
      shadowColor: ''
    },
    lineStyle: {
      type: 'solid',
      width: 2
    },
    areaStyle: {
      startColor: '',
      endColor: '',
      opacity: 1
    },
    smooth: false, //平滑曲线显示
    symbol: "circle", //标记的图形为实心圆
    symbolSize: 5, //标记的大小
    data: data
  }
};

export default {
  buildSeriesObj,
  buildDefaultSeriesObj
}
