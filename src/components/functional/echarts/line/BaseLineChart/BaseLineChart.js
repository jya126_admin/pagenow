import PnDesigner from "@/utils/PnDesigner";

const buildSeriesObj = function (name = '') {
  return {
    name: name,
    type: 'line',
    label: {
      show: false,
      position: 'inside',
      color: '#fff',
      fontSize: 12
    },
    itemStyle: {
      color: '',
      borderColor: '#fff',
      borderWidth: 0,
      borderType: 'solid',
      shadowBlur: 8,
      shadowColor: ''
    },
    lineStyle: {
      type: 'solid',
      width: 2
    },
    areaStyle: {
      show: true,
      normal: {
        startColor: '',
        endColor: '',
        shadowColor: '#1565c0',
        shadowBlur: 20
      }
    },
    smooth: true, //是否平滑曲线显示
    symbol: "circle", //标记的图形为实心圆
    symbolSize: 0,
    markLine: PnDesigner.buildMarkLineConfigData(),
    markPoint: PnDesigner.buildMarkPointConfigData()
  }
};

const buildDefaultSeriesObj = function (name = '', data) {
  return {
    name: name,
    type: 'line',
    label: {
      show: false,
      position: 'inside',
      color: '#fff',
      fontSize: 12
    },
    itemStyle: {
      color: '',
      borderColor: '#fff',
      borderWidth: 0,
      borderType: 'solid',
      shadowBlur: 8,
      shadowColor: ''
    },
    lineStyle: {
      type: 'solid',
      width: 2
    },
    areaStyle: {
      show: true,
      normal: {
        startColor: '',
        endColor: '',
        shadowColor: '#1565c0',
        shadowBlur: 20
      }
    },
    smooth: true, //是否平滑曲线显示
    symbol: "circle", //标记的图形为实心圆
    symbolSize: 0,
    data: data
  }
}

export default {
  buildSeriesObj,
  buildDefaultSeriesObj
}
