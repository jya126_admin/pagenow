import PnUtil from "@/utils/PnUtil";

/**
 * 构建热力图层配置数据
 * @param _id
 * @param _name
 * @returns
 */
const buildMapSeriesOption = function (_id = '', _name = '') {
  return {
    type: 'map',
    id: _id ? _id : PnUtil.uuid(),
    name: _name,
    geoIndex: 0,
    datasourceKey: '',
    data: [
      {name: '新疆维吾尔自治区', value: 950},
      {name: '甘肃省', value: 600},
      {name: '内蒙古自治区', value: 730},
      {name: '黑龙江省', value: 390},
      {name: '吉林省', value: 120},
      {name: '辽宁省', value: 190},
      {name: '广西壮族自治区', value: 430},
      {name: '广东省', value: 220},
      {name: '福建省', value: 100},
      {name: '江西省', value: 780},
      {name: '湖南省', value: 675},
      {name: '湖北省', value: 332},
      {name: '河南省', value: 880},
      {name: '安徽省', value: 573},
      {name: '浙江省', value: 847},
      {name: '上海市', value: 433},
      {name: '江苏省', value: 734},
      {name: '山东省', value: 234}
    ],
    tooltip: {
      position: 'inside',
      formatter: '',
      backgroundColor: 'rgba(50,50,50,0.7)',
      borderColor: '#333',
      borderWidth: 1,
      padding: 5
    }
  }
}

/**
 * 构建涟漪气泡层配置数据
 * @param _id
 * @param _name
 * @returns
 */
const buildEffectScatterSeriesOption = function (_id = '', _name = '') {
  return {
    type: 'effectScatter',
    id: _id ? _id : PnUtil.uuid(),
    name: _name,
    showEffectOn: 'render', // 'render' 绘制完成后显示特效；'emphasis' 高亮（hover）的时候显示特效
    rippleEffect: {
      color: '',
      period: 4,
      scale: 2.5,
      brushType: 'fill'
    },
    coordinateSystem: 'geo',
    geoIndex: 0,
    symbol: 'circle',
    symbolSize: 16,
    symbolSizeCallbackFuncCode: '(value, params) => {\n  return 16\n}',
    symbolOffset: ['0', '0'],
    label: {
      show: false,
      position: 'inside',
      distance: 5,
      rotate: 0,
      // formatter: '{@[2]}',
      labelFormatterCallbackFuncCode: '(params) => {\n  return \'{textColor|\'+params.value[2]+\'}\'\n}',
      color: '#fff',
      fontSize: 10,
      align: 'center',
      verticalAlign: 'middle',
      lineHeight: 0,
      backgroundColor: '',
      borderColor: '',
      borderWidth: 0,
      borderRadius: 0,
      padding: 0,
      shadowColor: '',
      shadowBlur: 0,
      rich: {
        textColor: {
          color: '#fff'
        }
      }
    },
    itemStyle: {
      color: '#ff9800',
      borderColor: '',
      borderWidth: 0,
      borderType: 'solid',
      shadowBlur: 0,
      shadowColor: ''
    },
    emphasis: {
      scale: true,
      itemStyle: {
        color: '#ef6c00',
        borderColor: '',
        borderWidth: 0,
        borderType: 'solid',
        shadowBlur: 0,
        shadowColor: ''
      }
    },
    tooltip: {
      position: 'right',
      // formatter: '',
      tooltipFormatterCallbackFuncCode: '(params) => {\n  return params.name + \'：\' + params.value[2]\n}',
      backgroundColor: 'rgba(50,50,50,0.7)',
      borderColor: '#333',
      borderWidth: 1,
      padding: 5
    },
    datasourceKey: '',
    data: [
      {name: '南宁市', value: [108.27331, 22.78121, 100]},
      {name: '广州市', value: [113.27324, 23.15792, 200]},
      {name: '福州市', value: [119.27345, 26.04769, 190]},
      {name: '温州市', value: [121.1572, 27.83616, 300]},
      {name: '北京市', value: [116.23128, 40.22077, 800]},
    ],
    zlevel: 1
  }
}

/**
 * 构建矢量散点层配置数据
 * @param _id
 * @param _name
 * @returns
 */
const buildScatterSeriesOption = function (_id = '', _name = '') {
  return {
    type: 'scatter',
    id: _id ? _id : PnUtil.uuid(),
    name: _name,
    coordinateSystem: 'geo',
    geoIndex: 0,
    symbol: 'circle',
    symbolSize: 16,
    symbolSizeCallbackFuncCode: '(value, params) => {\n  return 8\n}',
    symbolOffset: [0, 0],
    label: {
      show: false,
      position: 'inside',
      distance: 5,
      rotate: 0,
      // formatter: '{@[2]}',
      labelFormatterCallbackFuncCode: '(params) => {\n  return \'{textColor|\'+params.data.value[2]+\'}\'\n}',
      color: '#fff',
      fontSize: 10,
      align: 'center',
      verticalAlign: 'middle',
      lineHeight: 0,
      backgroundColor: '',
      borderColor: '',
      borderWidth: 0,
      borderRadius: 0,
      padding: 0,
      shadowColor: '',
      shadowBlur: 0,
      rich: {
        textColor: {
          color: '#fff'
        }
      }
    },
    itemStyle: {
      color: '#ff5722',
      borderColor: '',
      borderWidth: 0,
      borderType: 'solid',
      shadowBlur: 0,
      shadowColor: ''
    },
    emphasis: {
      scale: true,
      itemStyle: {
        color: '#e64a19',
        borderColor: '',
        borderWidth: 0,
        borderType: 'solid',
        shadowBlur: 0,
        shadowColor: ''
      }
    },
    tooltip: {
      position: 'right',
      // formatter: '',
      tooltipFormatterCallbackFuncCode: '(params) => {\n  return params.name + \'：\' + params.value[2]\n}',
      backgroundColor: 'rgba(50,50,50,0.7)',
      borderColor: '#333',
      borderWidth: 1,
      padding: 5
    },
    datasourceKey: '',
    data: [
      {"name":"北京","value":[116.405285,39.904989,524]},
      {"name":"天津","value":[117.190182,39.125596,13]},
      {"name":"上海","value":[121.472644,31.231706,140]},
      {"name":"重庆","value":[106.504962,29.533155,75]},
      {"name":"河北","value":[114.502461,38.045474,13]},
      {"name":"河南","value":[113.665412,34.757975,83]},
      {"name":"云南","value":[102.712251,25.040609,11]},
      {"name":"辽宁","value":[123.429096,41.796767,19]},
      {"name":"黑龙江","value":[126.642464,45.756967,15]},
      {"name":"湖南","value":[112.982279,28.19409,69]},
      {"name":"安徽","value":[117.283042,31.86119,260]},
      {"name":"山东","value":[117.000923,36.675807,39]},
      {"name":"新疆","value":[87.617733,43.792818,4]},
      {"name":"江苏","value":[118.767413,32.041544,31]},
      {"name":"浙江","value":[120.153576,30.287459,104]},
      {"name":"江西","value":[115.892151,28.676493,36]},
      {"name":"湖北","value":[114.298572,30.584355,1052]},
      {"name":"广西","value":[108.320004,22.82402,33]},
      {"name":"甘肃","value":[103.823557,36.058039,347]},
      {"name":"山西","value":[112.549248,37.857014,9]},
      {"name":"内蒙古","value":[111.670801,40.818311,157]},
      {"name":"陕西","value":[108.948024,34.263161,22]},
      {"name":"吉林","value":[125.3245,43.886841,4]},
      {"name":"福建","value":[119.306239,26.075302,18]},
      {"name":"贵州","value":[106.713478,26.578343,5]},
      {"name":"广东","value":[113.280637,23.125178,2398]},
      {"name":"青海","value":[101.778916,36.623178,41]},
      {"name":"西藏","value":[91.132212,29.660361,0]},
      {"name":"四川","value":[104.065735,30.659462,484]},
      {"name":"宁夏","value":[106.278179,38.46637,404]},
      {"name":"海南","value":[110.33119,20.031971,22]},
      {"name":"台湾","value":[121.509062,25.044332,3]},
      {"name":"香港","value":[114.173355,22.320048,5]},
      {"name":"澳门","value":[113.54909,22.198951,225]}
    ],
    zlevel: 1
  }
}

/**
 * 构建飞线层配置数据
 * @param _id
 * @param _name
 * @returns
 */
const buildLinesSeriesOption = function (_id = '', _name = '') {
  return {
    type: 'lines',
    id: _id ? _id : PnUtil.uuid(),
    name: _name,
    coordinateSystem: 'geo',
    geoIndex: 0,
    polyline: false,
    effect: {
      show: false,
      period: 10,
      delay: 0,
      constantSpeed: 30,
      symbol: 'triangle',
      symbolSize: 4,
      color: '#ffeb3b',
      trailLength: 0.7,
      loop: true
    },
    large: false,
    largeThreshold: 2000,
    symbol: 'circle',
    symbolSize: 0,
    lineStyle: {
      color: '#8bc34a',
      width: 2,
      type: 'solid',
      shadowBlur: 0,
      shadowColor: '',
      opacity: 0.2,
      curveness: 0.2
    },
    label: {
      show: false,
      position: 'middle',
      color: '#fff',
      fontSize: 10
    },
    datasourceKey: '',
    data: [
      {name: '', coords: [[118.8062,31.9208],[123.1238,42.1216]]},
      {name: '', coords: [[127.9688,45.368],[123.1238,42.1216]]},
      {name: '', coords: [[110.3467,41.4899],[123.1238,42.1216]]},
      {name: '', coords: [[125.8154,44.2584],[123.1238,42.1216]]},
      {name: '', coords: [[116.4551,40.2539],[123.1238,42.1216]]},
      {name: '', coords: [[119.4543,25.9222],[123.1238,42.1216]]},
      {name: '', coords: [[114.4995,38.1006],[123.1238,42.1216]]},
      {name: '', coords: [[117.4219,39.4189],[123.1238,42.1216]]},
      {name: '', coords: [[112.3352,37.9413],[123.1238,42.1216]]},
      {name: '', coords: [[109.1162,34.2004],[123.1238,42.1216]]},
      {name: '', coords: [[103.5901,36.3043],[123.1238,42.1216]]},
      {name: '', coords: [[106.3586,38.1775],[123.1238,42.1216]]},
      {name: '', coords: [[101.4038,36.8207],[123.1238,42.1216]]},
      {name: '', coords: [[103.9526,30.7617],[123.1238,42.1216]]},
      {name: '', coords: [[108.384366,30.439702],[123.1238,42.1216]]},
      {name: '', coords: [[113.0823,28.2568],[123.1238,42.1216]]},
      {name: '', coords: [[102.9199,25.46639],[123.1238,42.1216]]},
      {name: '', coords: [[91.11,29.97],[123.1238,42.1216]]},
      {name: '', coords: [[87.68,43.77],[123.1238,42.1216]]}
    ],
    zlevel: 2
  }
}

/**
 * 构建点热力层配置数据
 * @param _id
 * @param _name
 * @returns
 */
const buildHeatmapSeriesOption = function (_id = '', _name = '') {
  return {
    type: 'heatmap',
    id: _id ? _id : PnUtil.uuid(),
    name: _name,
    coordinateSystem: 'geo',
    geoIndex: 0,
    pointSize: 10,
    blurSize: 8,
    minOpacity: 0,
    manOpacity: 1,
    datasourceKey: '',
    data: [
      [116.405285,39.904989,100],
      [117.190182,39.125596,458],
      [121.472644,31.231706,19],
      [106.504962,29.533155,307],
      [114.502461,38.045474,42],
      [113.665412,34.757975,372],
      [102.712251,25.040609,249],
      [123.429096,41.796767,258],
      [126.642464,45.756967,116],
      [112.982279,28.19409,152],
      [117.283042,31.86119,229],
      [117.000923,36.675807,41],
      [87.617733,43.792818,493],
      [118.767413,32.041544,442],
      [120.153576,30.287459,319],
      [115.892151,28.676493,21],
      [114.298572,30.584355,12],
      [108.320004,22.82402,206],
      [103.823557,36.058039,107],
      [112.549248,37.857014,467],
      [111.670801,40.818311,449],
      [108.948024,34.263161,200],
      [125.3245,43.886841,436],
      [119.306239,26.075302,471],
      [106.713478,26.578343,16],
      [113.280637,23.125178,276],
      [101.778916,36.623178,303],
      [91.132212,29.660361,321],
      [104.065735,30.659462,494],
      [106.278179,38.46637,315],
      [110.33119,20.031971,326],
      [121.509062,25.044332,238],
      [114.173355,22.320048,36],
      [113.54909,22.198951,440]
    ],
    zlevel: 1
  }
}

/**
 * 构建连续型视觉映射组件配置数据
 * @param _id
 * @param _min
 * @param _max
 * @param _inRangeColor
 * @param _seriesIndex
 * @returns
 */
const buildContinuousVisualMapOption = function (
  _id = '',
  _min = 0,
  _max = 1000,
  _color = [],
  _seriesIndex = null
) {
  return {
    id: _id ? _id : PnUtil.uuid(),
    show: false,
    min: _min,
    max: _max,
    range: [_min, _max], // 指定手柄对应数值的位置
    calculable: false, // 是否显示拖拽用的手柄
    realtime: true, // 拖拽时，是否实时更新
    precision: 0, // 数据展示的小数精度，默认为0，无小数点
    orient: 'vertical', // 如何放置 visualMap 组件
    inRange: {
      color: _color
    },
    seriesIndex: _seriesIndex
  }
}

export default {
  buildMapSeriesOption,
  buildScatterSeriesOption,
  buildEffectScatterSeriesOption,
  buildLinesSeriesOption,
  buildContinuousVisualMapOption,
  buildHeatmapSeriesOption
}