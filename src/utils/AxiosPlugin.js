import axios from 'axios'
import {Message, LoadingBar} from 'view-design'

import router from '../router'

// 创建 axios 实例
// 这里 export 的原因是方便组件外使用 axios
export const Axios = axios.create({
  //baseURL: process.env.BASE_URL,
  baseURL: window.g.AXIOS_BASE_URL,
  timeout: 100000,
});

// POST传参序列化(添加请求拦截器)
// 在发送请求之前处理相关逻辑
Axios.interceptors.request.use(config => {
  LoadingBar.start();

  // 设置以 form 表单的形式提交参数，如果以 JSON 的形式提交表单，可忽略
  if (config.method === 'post' && config.headers['Content-Type'] === 'application/x-www-form-urlencoded') {
    let ret = '';
    for (let it in config.data) {
      ret += encodeURIComponent(it) + '=' + encodeURIComponent(config.data[it]) + '&'
    }
    config.data = ret;
  }

  const token = localStorage.getItem('token');
  if (token) {
    config.headers['Authorization'] = token;
  }
  return config
}, error => {
  return Promise.reject(error)
});

// 返回状态判断(添加响应拦截器)
Axios.interceptors.response.use(res => {
  LoadingBar.finish();

  // 当接口返回的响应头信息中存在authorization时，更新本地token
  if (res.headers.authorization) {
    localStorage.setItem('token', res.headers.authorization)
  }

  return res
}, error => {
  LoadingBar.error();

  if (error.response.status === 401) {
    // 401 说明 token 验证失败
    // 可以直接跳转到登录页面，重新登录获取 token

    // Message.error(error.response.data.message);
    localStorage.removeItem('token');
    localStorage.removeItem('current_user');
    router.replace({
      path: '/login'
    })
  } else if (error.response.status === 403) {
    Message.error(error.response.data.message);
  } else if (error.response.status === 400) {
    Message.error(error.response.data.message)
  }
  // 返回 response 里的错误信息
  // Message.error({
  //   content: error.response.data.message,
  //   duration: 8,
  //   closable: true
  // })
  return Promise.reject(error.response.data)
});

export default {
  Axios
}

