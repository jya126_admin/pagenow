import PnUtil from "@/utils/PnUtil";

const buildFormLayoutConfigData = function () {
  let layoutConfigData = {
    formTheme: 'black', // 表单主题：暗黑系black、默认default

    backgroundColor: '#0E2B43',
    syncSetPageBgColor: true, // 是否同步设置页面背景色
    padding: 10,

    formConfig: {
      formRefsName: 'pnForm',
      formData: '{\n  \n}', // 表单数据
      inline: false, // 是否开启行内表单模式
      labelPosition: 'right', // 表单域标签的位置，可选值为 left、right、top
      labelWidth: 100, // 表单域标签的宽度，所有的 FormItem 都会继承 Form 组件的 label-width 的值
      showMessage: true, // 是否显示校验错误信息
    }

  };
  return layoutConfigData
};

const buildFormWidgetLayoutItemConfigData = function () {
  return {

  }
}

const buildGridColContainerLayoutItem = function () {
  let obj = {
    id: PnUtil.uuid(),
    name: 'FormWidgetLayoutItem',
    aliasName: '栅格列',
    layoutItemConfigData: buildFormWidgetLayoutItemConfigData(),
    component: {
      id: PnUtil.uuid(),
      name: 'GridColWidget',
      version: '1.0',
      container: true,
      compConfigData: {
        className: '',
        span: 12,
        offset: 0,
        push: 0,
        pull: 0,
        xs: 24,
        sm: 12,
        md: 12,
        lg: 12,
        xl: 12,
        xxl: 12,
        styleConfig: {
          backgroundColor: '',
          padding: 0,
          border: {
            leftBorderWidth: 0,
            leftBorderStyle: 'solid',
            leftBorderColor: '#ccc',
            topBorderWidth: 0,
            topBorderStyle: 'solid',
            topBorderColor: '#ccc',
            rightBorderWidth: 0,
            rightBorderStyle: 'solid',
            rightBorderColor: '#ccc',
            bottomBorderWidth: 0,
            bottomBorderStyle: 'solid',
            bottomBorderColor: '#ccc',
          }
        }
      }
    },
    children: []
  }
  return obj
}

const buildGridContainerLayoutItem = function () {
  let obj = {
    id: PnUtil.uuid(),
    name: 'FormWidgetLayoutItem',
    aliasName: '栅格',
    layoutItemConfigData: buildFormWidgetLayoutItemConfigData(),
    component: {
      id: PnUtil.uuid(),
      name: 'GridWidget',
      version: '1.0',
      container: true,
      compConfigData: {
        gutter: 0,
        type: '',
        align: '',
        justify: '',
        wrap: true
      },
    },
    children: [
      buildGridColContainerLayoutItem(),
      buildGridColContainerLayoutItem(),
    ]
  }
  return obj
}

export default {
  buildFormLayoutConfigData,
  buildFormWidgetLayoutItemConfigData,
  buildGridColContainerLayoutItem,
  buildGridContainerLayoutItem,

}
