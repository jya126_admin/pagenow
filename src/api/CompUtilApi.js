import {Axios} from '../utils/AxiosPlugin'

const BASE_URI = 'http://localhost:3325';

/**
 * 创建组件相关文件
 * @param componentName 组件名
 * @param componentAliasName 组件别名
 * @param componentType 组件类型
 * @param rootFolder 存放的根目录
 * @returns {Promise<AxiosResponse<T>>}
 */
const createCompFiles = async function (componentName, componentAliasName, componentType, rootFolder) {
  return await Axios.post(BASE_URI + '/createCompFiles', {
    componentName: componentName,
    componentAliasName: componentAliasName,
    componentType: componentType,
    rootFolder: rootFolder
  });
};

export default {
  createCompFiles
}
