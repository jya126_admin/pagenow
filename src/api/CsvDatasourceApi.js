import {Axios} from '../utils/AxiosPlugin'

const saveCsvDatasource = async function (csvDatasource) {
  return await Axios.post('/csvDatasource/saveCsvDatasource', csvDatasource);
};

const getAllCsvDatasource = async function (project_id) {
  return await Axios.get('/csvDatasource/getAllCsvDatasource', {params: {project_id: project_id}});
};

const getCsvDatasourceByPage = async function (pageIndex, pageSize, name) {
  return await Axios.post('/csvDatasource/getCsvDatasourceByPage', {pageIndex: pageIndex, pageSize: pageSize, name: name});
};

const getCsvDatasourceById = async function (id) {
  return await Axios.get('/csvDatasource/getCsvDatasourceById', {params: {id: id}})
};

const getCsvDatasourceByName = async function (name) {
  return await Axios.get('/csvDatasource/getCsvDatasourceByName', {params: {name: name}});
};

const getContentToArray = async function (fileName, folderName) {
  return await Axios.get('/csvDatasource/getContentToArray', {params: {fileName: fileName, folderName: folderName}});
};

const deleteCsvDatasource = async function (id) {
  return await Axios.delete('/csvDatasource/deleteCsvDatasource', {params: {id: id}});
};

const parseCsvOrExcel = async function (fileName, folderName) {
  return await Axios.get('/csvDatasource/parseCsvOrExcel', {params: {fileName: fileName, folderName: folderName}});
}

export default {
  saveCsvDatasource,
  getAllCsvDatasource,
  getCsvDatasourceByPage,
  getCsvDatasourceById,
  getCsvDatasourceByName,
  getContentToArray,
  deleteCsvDatasource,
  parseCsvOrExcel
}
