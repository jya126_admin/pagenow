import {Axios} from '../utils/AxiosPlugin'

const saveShareCustomComp = async function (shareCustomComp) {
  return await Axios.post('/shareCustomComp/saveShareCustomComp', shareCustomComp);
};

const getShareCustomCompByPage = async function (pageIndex, pageSize, name, is_share) {
  return await Axios.post('/shareCustomComp/getShareCustomCompByPage', {pageIndex: pageIndex, pageSize: pageSize, name: name, is_share: is_share});
};

const getMyShareCustomCompByPage = async function (pageIndex, pageSize, name) {
  return await Axios.post('/shareCustomComp/getMyShareCustomCompByPage', {pageIndex: pageIndex, pageSize: pageSize, name: name});
};

const getShareCustomCompById = async function (id) {
  return await Axios.get('/shareCustomComp/getShareCustomCompById', {params: {id: id}});
}

export default {
  saveShareCustomComp,
  getShareCustomCompByPage,
  getMyShareCustomCompByPage,
  getShareCustomCompById
}
