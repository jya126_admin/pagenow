
import { getField, updateField } from 'vuex-map-fields';

const state = {

}

const getters = {
  getField
}

const mutations = {
  updateField
}

export default {
  namespaced: true,
  state,
  getters,
  mutations
}
